# portfolio_5513231
First Flutter projekt

Name: Jiaxu Shen

Email: jxshen2009@outlook.com 

Matrikelnummer: 5513231


## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


## Description

To do:

Ziel des Praktikums:
Das Ziel dieses Praktikums ist die Einrichtung einer voll funktionsfähigen Entwicklungsumgebung für Flutter sowie die
korrekte Einrichtung eines Git-Repositories, zu dem der Dozen eingeladen wird. Dies dient als Grundlage für die
zukünftige Arbeit und Bewertung im Rahmen des Kurses.

Abgabedatum:
Fertigstellung und Einreichung bis zum 26. April 2024, 12:00 Uhr. Bei fristgerechter und korrekter Abgabe werden 3
Punkte vergeben.

Aufgaben:
1. Installation der Entwicklungsumgebung

-Installiere Flutter auf Deinem Computer. Folge der offiziellen Installationsanleitung auf der Flutter Webseite.
-Stelle sicher, dass die Flutter-Umgebung richtig konfiguriert ist, indem Du den Befehl flutter doctor im
Terminal ausführst. Korrigiere eventuelle Fehler oder fehlende Komponenten, bis der Befehl keine relevanten
Fehler mehr anzeigt.

2. Einrichtung des Git-Repositories

-Erstelle ein neues Repository auf GitHub, GitLab oder einer ähnlichen Plattform. Der Name des Repositories muss
portfolio_<matrnr> sein, wobei <matrnr> durch Deine Matrikelnummer zu ersetzen ist.
-Lade mich per E-Mail in Dein Git-Repository ein. Verwende dazu die E-Mail-Adresse des Dozenten.

3. Erstellung einer README-Datei und Initialisierung des Flutter-Projekts

-Füge im Hauptverzeichnis Deines Repositories eine README.md Datei hinzu. Diese sollte eine kurze
Beschreibung des Projektes sowie Anweisungen zur Installation und Ausführung enthalten.
-Initialisiere innerhalb des Repositories ein neues Flutter-Projekt mit dem Namen portfolio_<matrnr> .
Verwende dazu den Befehl:
"flutter create portfolio_<matrnr>"
